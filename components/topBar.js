import React, { Component } from "react";
import styled from "styled-components";

export default class TopBar extends Component {
  render() {
    return (
      <Bar>
        <Logo source={require("./../assets/logo-3x.png")} />
      </Bar>
    );
  }
}

const Bar = styled.View`
  width: 100%;
  height: 65px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const Logo = styled.Image``;

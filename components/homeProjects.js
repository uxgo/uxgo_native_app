import React, { Component } from "react";
import styled from "styled-components";
import TopBar from "./topBar";
import BottomBar from "./bottomBar";

export default class HomeProjects extends Component {
  render() {
    return (
      <HomeView>
        <TopBar />
        <NPImage source={require("./../assets/no-projects-image-3x.png")} />
        <NPCTA>Press “+” to create your first project</NPCTA>
        <StartButton onPress={this.props.handlePress}>
          Start scanning
        </StartButton>
        <BottomBar />
      </HomeView>
    );
  }
}

// Styled components
const HomeView = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const NPCTA = styled.Text`
  font-size: 20px;
  color: blue;
`;

const NPImage = styled.Image`
  width: 197px;
  height: 158px;
`;

const StartButton = styled.Text`
  font-size: 24px;
  width: 100%;
  color: blue;
  text-align: center;
`;

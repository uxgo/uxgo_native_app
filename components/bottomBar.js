import React, { Component } from "react";
import styled from "styled-components";

export default class BottomBar extends Component {
  render() {
    return (
      <Bar>
        <Text> Projects </Text>
        <Text> + </Text>
        <Text> Settings </Text>
      </Bar>
    );
  }
}

// Styled components
const Bar = styled.View`
  width: 100%;
  height: 50px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

const Text = styled.Text`
  font-size: 12px;
  color: blue;
`;

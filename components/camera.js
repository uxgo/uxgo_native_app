import React, { Component, Fragment } from "react";
import { Text, View, TouchableOpacity, Button } from "react-native";
import { RNCamera } from "react-native-camera";
import styled from "styled-components";

export default class Camera extends Component {
  render() {
    return (
      <Fragment>
        <RNCamera
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          permissionDialogTitle={"Permission to use camera"}
          permissionDialogMessage={
            "UXGO needs your permission to use the camera phone"
          }
          onBarCodeRead={this.onBarcodeRead}
        >
          <CameraView>
            <Overlay>
              <StopButton onPress={this.props.handlePress}>STOP</StopButton>
            </Overlay>
          </CameraView>
        </RNCamera>
      </Fragment>
    );
  }
}

const CameraView = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Overlay = styled.TouchableOpacity`
  background-color: rgba(255, 255, 255, 0.3);
  width: 82px;
  height: 82px;
  padding: 10px 20px;
  border-radius: 100px;
  position: absolute;
  bottom: 28px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const StopButton = styled.Text`
  font-size: 18px;
  color: white;
  padding: 0;
  margin: 0;
`;

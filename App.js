import React, { Component, Fragment } from "react";
import { Text, View, TouchableOpacity, Button } from "react-native";
import styled from "styled-components";
import { RNCamera } from "react-native-camera";
// Components
import HomeProjects from "./components/homeProjects";
import Camera from "./components/camera";

export default class App extends Component {
  state = {
    isScanning: false
  };
  //
  onBarcodeRead = data => {
    console.log(data);
    alert(data.data);
  };
  //
  handlePress = () => {
    this.state.isScanning
      ? this.setState({ isScanning: false })
      : this.setState({ isScanning: true });
  };

  render() {
    return (
      <View>
        {this.state.isScanning ? (
          <Camera handlePress={this.handlePress} />
        ) : (
          <HomeProjects handlePress={this.handlePress} />
        )}
      </View>
    );
  }
}

// Styled components
const MainView = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
